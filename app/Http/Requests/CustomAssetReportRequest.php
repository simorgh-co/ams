<?php

namespace App\Http\Requests;

class CustomAssetReportRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'purchase_start'        => 'date|nullable',
            'purchase_end'          => 'date|nullable',
            'created_start'         => 'date|nullable',
            'created_end'           => 'date|nullable',
            'checkout_date_start'   => 'date|nullable',
            'checkout_date_end'     => 'date|nullable',
            'expected_checkin_start'      => 'date|nullable',
            'expected_checkin_end'        => 'date|nullable',
            'checkin_date_start'      => 'date|nullable',
            'checkin_date_end'        => 'date|nullable',
            'last_audit_start'      => 'date|nullable',
            'last_audit_end'        => 'date|nullable',
            'next_audit_start'      => 'date|nullable',
            'next_audit_end'        => 'date|nullable',
        ];
    }

    public function response(array $errors)
    {
        return $this->redirector->back()->withInput()->withErrors($errors, $this->errorBag);
    }
}
