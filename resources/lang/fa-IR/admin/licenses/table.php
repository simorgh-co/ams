<?php

return array(

    'assigned_to'   	=> 'اختصاص داده شده به',
    'checkout'   		=> 'تحویل دادن',
    'id'      			=> 'شناسه',
    'license_email'   	=> 'ایمیل لایسنس',
    'license_name'   	=> 'اختصاص داده شده به',
    'purchase_date'   	=> 'تاریخ خرید',
    'purchased'   		=> 'خریداری شده',
    'seats'   			=> 'صندلی‌ها',
    'hardware'   		=> 'سخت‌افزار',
    'serial'   			=> 'سریال',
    'title'      		=> 'لایسنس',

);
