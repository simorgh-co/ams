<?php

return [

'invalid_return_count' => 'تعداد نامعتبری از :name بازگشت داده شده است. :expected انتظار می رود، در حالی که :actual دریافت شده است.',
    'invalid_return_type'  => 'نوع بازگشتی از :name نامعتبر است. :expected انتظار می رود، در حالی که :actual دریافت شده است.',
    'invalid_return_value' => 'مقدار بازگشتی از :name نامعتبر است. :expected انتظار می رود، در حالی که :actual دریافت شده است.',
    'does_not_exist' => 'برچسب وجود ندارد',
    
];
