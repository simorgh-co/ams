<?php

return [

'labels_per_page'    => 'برچسب‌ها',
    'support_fields'     => 'فیلدها',
    'support_asset_tag'  => 'برچسب دارایی',
    'support_1d_barcode' => 'بارکد یک بعدی',
    'support_2d_barcode' => 'بارکد دو بعدی',
    'support_logo'       => 'لوگو',
    'support_title'      => 'عنوان',

];