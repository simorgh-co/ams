<?php

return [
    'about_asset_depreciations'  			=> 'درباره‌ی استهلاک دارایی',
    'about_depreciations'  					=> 'شما می‌توانید استهلاک دارایی را فعال کنید تا دارایی‌ها را بر اساس استهلاک خط مستقیم، مستهلک کنید.',
    'asset_depreciations'  					=> 'استهلاک دارایی',
    'create'  					            => 'ایجاد استهلاک',
    'depreciation_name'  					=> 'نام استهلاک',
    'depreciation_min'                      => 'کف ارزش استهلاک',
    'number_of_months'  					=> 'تعداد ماه',
    'update'  					            => 'آپدیت استهلاک',
    'depreciation_min'                      => 'حداقل ارزش پس از استهلاک',
    'no_depreciations_warning'               => '<strong>هشدار: </strong>
                      شما در حال حاضر هیچ استهلاکی تنظیم نکرده‌اید.
                      لطفاً برای مشاهده گزارش استهلاک، حداقل یک استهلاک تنظیم کنید.',
];
