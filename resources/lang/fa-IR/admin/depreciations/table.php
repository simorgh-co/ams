<?php

return [

    'id'      => 'شناسه',
    'months'   => 'ماه(ها)',
    'term'   => 'دوره',
    'title'      => 'نام ',
    'depreciation_min' => 'کف ارزش',

];
