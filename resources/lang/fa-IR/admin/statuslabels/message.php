<?php

return [

    'does_not_exist' => 'برچسب وضعیت موجود نیست',
    'assoc_assets'	 => 'این برچسب وضعیت در حال حاضر حداقل با یک دارایی همراه است و نمی‌تواند حذف شود. لطفا دارایی‌های خود را به روز و دوباره امتحان کنید.',

    'create' => [
        'error'   => 'برچسب وضعیت ایجاد نشد، لطفا دوباره امتحان کنید.',
        'success' => 'برچسب وضعیت با موفقیت ایجاد شد.',
    ],

    'update' => [
        'error'   => 'برچسب وضعیت به روز نشده است، لطفا دوباره امتحان کنید.',
        'success' => 'برچسب وضعیت با موفقیت به روز شد.',
    ],

    'delete' => [
        'confirm'   => 'آیا مطمئنید که می‌خواهید این برچسب وضعیت را حذف کنید؟',
        'error'   => 'یک مسئله برای حذف برچسب وضعیت وجود داشت. لطفا دوباره تلاش کنید.',
        'success' => 'برچسب وضعیت با موفقیت حذف شد.',
    ],

    'help' => [
        'undeployable'   => 'این دارایی‌ها را نمی‌توان به کسی اختصاص داد.',
        'deployable'   => 'این دارایی‌ها قابل تحویل هستند. هنگامی که آن‌ها تحویل داده شدند، وضعیت متا <i class="fas fa-circle text-blue"></i> <strong>Deployed</strong> نظر گرفته می‌شود.',
        'archived'   => 'این دارایی ها قابل تحویل نیستند و فقط در فهرست آرشیو نشان داده می‌شوند. این برای حفظ اطلاعات دارایی‌ها جهت اهداف بودجه بندی/سوابق مفید است اما آن‌ها را از لیست دارایی‌های روزانه دور نگه می‌دارد.',
        'pending'   => 'این دارایی‌ها هنوز نمی‌توانند به کسی واگذار شوند، اغلب برای اقلامی استفاده می‌شوند که برای تعمیر خارج شده‌اند، اما انتظار می‌رود دوباره به گردش درآیند.',
    ],

];
