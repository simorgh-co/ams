<?php

return array(

    'does_not_exist' => 'مواد مصرفی یافت نشد.',

    'create' => array(
        'error'   => 'مواد مصرفي ایجاد نشد, لطفا دوباره سعی کنید.',
        'success' => 'مواد مصرفی ایجاد شد.'
    ),

    'update' => array(
        'error'   => 'مواد مصرفي بروز رسانی نشد, لطفا دوباره سعی کنید',
        'success' => 'مواد مصرفي با موفقیت به روز رسانی شد.'
    ),

    'delete' => array(
        'confirm'   => 'آیا مطمعنید که می‌خواهید این ماده مصرفی را حذف کنید؟',
        'error'   => 'یک مشکل در حذف کردن ماده مصرفی وجود دارد، دوباره سعی کنید.',
        'success' => 'مواد مصرفی حذف شد.'
    ),

     'checkout' => array(
        'error'   		=> 'مواد مصرفی تحویل داده نشد، دوباره سعی کنید.',
        'success' 		=> 'مواد مصرفی تحویل داده شد.',
        'user_does_not_exist' => 'کاربر نامعتبر است لطفا دوباره امتحان کنید.',
         'unavailable'      => 'میزان موجودی برای تحویل دادن کافی نیست.',
    ),

    'checkin' => array(
        'error'   		=> 'مواد مصرفی تحویل گرفته نشد ، دوباره سعی کنید',
        'success' 		=> 'مواد مصرفی تحویل گرفته شد.',
        'user_does_not_exist' => 'کاربر نامعتبر است لطفا دوباره امتحان کنید.'
    )


);
