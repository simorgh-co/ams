<?php

return array(
    'checkout'                          => 'تحویل دادن مصرفی به کاربر',
    'consumable_name'                   => 'نام مصرفی',
    'create'                            => 'ایجاد مواد مصرفی',
    'item_no'                           => 'شماره مورد ',
    'remaining' 			            => 'یاقیمانده',
    'total' 			                => 'مجموع',
    'update'                            => 'به روز رسانی مواد مصرفی',
);
