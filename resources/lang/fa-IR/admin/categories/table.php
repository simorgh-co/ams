<?php

return array(
	'eula_text'      			=> 'ضمانت‌نامه کاربری',
    'id'      					=> 'شناسه',
    'parent'   					=> 'مجموعه پدر',
    'require_acceptance'      	=> 'پذیرش',
    'title'      				=> 'نام دسته دارایی',

);
