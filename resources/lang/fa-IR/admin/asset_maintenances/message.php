<?php

    return [
        'not_found'                    => 'تعمیر و نگهداری که جستجو می‌کنید یافت نشد.',
        'delete'                       => [
            'confirm' => 'آیا مطمئن هستید می‌خواهید این آیتم حذف شود؟',
            'error'   => 'اشکال در حذف تعمیر و نگهداری دارایی. لطفا دوباره امتحان کنید.',
            'success' => 'تعمیر و نگهداری دارایی با موفقیت حذف شد.',
        ],
        'create'                       => [
            'error'   => 'تعمیر و نگهداری دارایی ایجاد نشد. لطفا دوباره امتحان کنید.',
            'success' => 'تعمیر و نگهداری دارایی با موفقیت ایجاد شد.',
        ],
        'edit'                       => [
            'error'   => 'تعمیر و نگهداری دارایی ویرایش نشد، لطفا دوباره امتحان کنید.',
            'success' => 'تعمیر و نگهداری دارایی با موفقیت ویرایش شد.',
        ],
        'asset_maintenance_incomplete' => 'هنوز تکمیل نشده',
        'warranty'                     => 'گارانتی',
        'not_warranty'                 => 'بدون گارانتی',
    ];
