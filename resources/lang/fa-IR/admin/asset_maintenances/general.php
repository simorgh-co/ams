<?php

    return [
        'asset_maintenances' => 'تعمیر و نگهداری دارایی',
        'edit'               => 'ویرایش تعمیر و نگهداری دارایی',
        'delete'             => 'حذف تعمیر و نگهداری دارایی',
        'view'               => 'نمایش جزییات تعمیر و نگهداری دارایی',
        'repair'             => 'بازسازی',
        'maintenance'        => 'تعمیر و نگهداری',
        'upgrade'            => 'ارتقا',
        'calibration'        => 'کالیبره کردن',
        'software_support'   => 'پشتیبانی نرم‌افزاری',
        'hardware_support'   => 'پشتیبانی سخت‌افزاری',
        'configuration_change'   => 'تغییرات پیکربندی:',
        'pat_test'           => 'تست PAT',
    ];
