<?php

    return [
        'asset_maintenance_type' => 'نوع تعمیر و نگهداری',
        'title'                  => 'عنوان',
        'start_date'             => 'تاریخ شروع',
        'completion_date'        => 'تاریخ پایان',
        'cost'                   => 'هزینه',
        'is_warranty'            => 'گارانتی',
        'asset_maintenance_time' => 'مدت زمان کار (به روز)',
        'notes'                  => 'توضیحات',
        'update'                 => 'آپدیت',
        'create'                 => 'ایجاد تعمیر و نگهداری'
    ];
