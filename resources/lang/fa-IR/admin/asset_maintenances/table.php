<?php

    return [
        'title'         => 'تعمیر و نگهداری دارایی',
        'asset_name'    => 'نام دارایی',
        'is_warranty'   => 'گارانتی',
        'dl_csv'        => 'دانلود CSV',
    ];
