<?php
return array(
    'companies' => 'شرکت‌ها',
    'create'    => 'ایجاد شرکت',
    'title'     => 'شرکت',
    'update'    => 'آپدیت شرکت',
    'name'      => 'نام شرکت',
    'id'        => 'شناسه',
);
