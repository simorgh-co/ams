<?php

return array(

    'support_url_help' => 'متغیرهای <code>{LOCALE}</code>، <code>{SERIAL}</code>، <code>{MODEL_NUMBER}</code> و <code>{MODEL_NAME}</code> ممکن است برای اینکه آن مقادیر به صورت خودکار پر شوند، هنگام مشاهده دارایی‌ها، در URL شما استفاده شوند.  - برای مثال https://support.apple.com/{LOCALE}/{SERIAL}.',
    'does_not_exist' => 'سازنده وجود ندارد.',
    'assoc_users'	 => 'این سازنده در حال حاضر با حداقل یک مدل همراه است و نمی‌تواند حذف شود. لطفا مدل‌های خود را به سازنده دیگری تغییر دهید و دوباره امتحان کنید.',

    'create' => array(
        'error'   => 'سازنده ایجاد نشد، لطفا دوباره امتحان کنید.',
        'success' => 'سازنده با موفقیت ایجاد شد.'
    ),

    'update' => array(
        'error'   => 'سازنده به روز رسانی نشد، لطفا دوباره امتحان کنید',
        'success' => 'سازنده با موفقیت به روز رسانی شد.'
    ),

    'restore' => array(
        'error'   => 'سازنده بازیابی نشد، لطفاً دوباره امتحان کنید',
        'success' => 'سازنده بازیابی شد، لطفاً دوباره امتحان کنید'
    ),

    'delete' => array(
        'confirm'   => 'آیا شما مطمئن هستید که می‌خواهید این سازنده را حذف کنید؟',
        'error'   => 'مسئله‌ای در حذف سازنده وجود دارد. لطفا دوباره تلاش کنید.',
        'success' => 'سازنده با موفقیت حذف شد.'
    )

);
