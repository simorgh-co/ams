<?php

return array(

    'id'                        => 'شناسه',
    'name'                      => 'نام دپارتمان',
    'manager'                   => 'مدیر',
    'location'                  => 'محل',
    'create'                    => 'ایجاد دپارتمان',
    'update'                    => 'آپدیت دپارتمان',
    );
