<?php

return [
    'info'   => 'گزینه‌ای را که برای گزارش دارایی خود می‌خواهید انتخاب کنید .',
    'deleted_user' => 'حذف کاربر',
    'send_reminder' => 'ارسال يادآوری',
    'reminder_sent' => 'یادآوری ارسال شد',
    'acceptance_deleted' => 'درخواست پذیرش حذف شد',
    'acceptance_request' => 'درخواست پذیرش'
];