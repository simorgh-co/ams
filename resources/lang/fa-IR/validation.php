<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'             => ':attribute باید تایید شود.',
    'active_url'           => ':attribute یک URL معتبر نیست.',
    'after'                => ':attribute باید در تاریخی بعد از :date باشد.',
    'after_or_equal'       => ':attribute باید یک تاریخ بعد یا برابر :date باشد.',
    'alpha'                => ':attribute می‌تواند فقط شامل حروف باشد.',
    'alpha_dash'           => ':attribute می‌تواند فقط شامل حروف، اعداد و خط های فاصله باشد.',
    'alpha_num'            => ':attribute می‌تواند فقط شامل حروف و اعداد باشد.',
    'array'                => ':attribute باید یک آرایه باشد.',
    'before'               => ':attribute باید در تاریخی قبل از :date باشد.',
    'before_or_equal'      => 'attribute باید تاریخ قبل یا برابر :date باشد.',
    'between'              => [
        'numeric' => ':attribute باید بین حداقل حداکثر باشد.',
        'file'    => ':attribute باید بین حداقل حداکثر کیلوبایت باشد.',
        'string'  => ':attribute باید بین حداقل حداکثر کاراکتر باشد.',
        'array'   => 'attribute باید بین: min و: max items باشد.',
    ],
    'boolean'              => 'فیلد :attribute باید true یا false باشد.',
    'confirmed'            => 'تایید :attribute منطبق نیست.',
    'date'                 => 'تاریخ :attribute معتبر نیست.',
    'date_format'          => ':attribute منطبق بر شکل :format نیست.',
    'different'            => ':attribute و :other باید متفاوت باشد.',
    'digits'               => ':attribute باید رقم :digits باشد.',
    'digits_between'       => ':attribute باید بین :min و :max رقم باشد.',
    'dimensions'           => 'attribute: ابعاد تصویر نامعتبر است.',
    'distinct'             => 'فیلد :attribute دارای مقدار تکراری است.',
    'email'                => 'فرمت :attribute نامعتبر است.',
    'exists'               => ':attribute انتخاب شده نامعتبر است.',
    'file'                 => ':attribute باید یک فایل باشد.',
    'filled'               => 'فیلد :attribute باید مقدار داشته باشد.',
    'image'                => ':attribute باید یک عکس باشد.',
    'import_field_empty'    => 'مقدار :fieldname نباید خالی باشد.',
    'in'                   => ':attribute انتخاب شده نامعتبر است.',
    'in_array'             => 'فیلد :attribute در هیچ :other دیگری وجود ندارد.',
    'integer'              => ':attribute باید یک عدد باشد.',
    'ip'                   => ':attribute باید یک آدرس IP معتبر باشد.',
    'ipv4'                 => ':attribute باید یک آدرس IPv4 معتبر باشد.',
    'ipv6'                 => ':attribute باید یک آدرس IPv6 معتبر باشد.',
    'is_unique_department' => ':attribute باید منحصر به مکان این شرکت باشد',
    'json'                 => ':attribute باید یک رشته معتبر JSON باشد.',
    'max'                  => [
        'numeric' => ':attribute نباید بزرگتر از :max باشد.',
        'file'    => ':attribute نباید بزرگتر از :max کیلوبایت باشد.',
        'string'  => ':attribute نباید بزرگتر از :max کاراکتر باشد.',
        'array'   => ':attribute می‌تواند بیش از :max مورد داشته باشد.',
    ],
    'mimes'                => ':attribute باید فایلی از نوع :values باشد.',
    'mimetypes'            => ':attribute باید یک فایل از نوع :values ​​باشد.',
    'min'                  => [
        'numeric' => ':attribute باید حداقل :min باشد.',
        'file'    => ':attribute باید حداقل :min کیلوبایت باشد.',
        'string'  => ':attribute باید حداقل :min کاراکتر باشد.',
        'array'   => ':attribute: باید دارای حداقل :min مورد باشد.',
    ],
    'starts_with'          => ':attribute : باید با یکی از موارد زیر شروع شود :values.',
    'ends_with'            => ':attribute : باید به یکی از موارد زیر ختم شود :values',

    'not_in'               => ':attribute انتخاب شده نامعتبر است.',
    'numeric'              => ':attribute باید عدد باشد.',
    'present'              => 'فیلد attribute باید باشد.',
    'valid_regex'          => 'این یک regex معتبر نیست.',
    'regex'                => 'شکل :attribute نامعتبر است.',
    'required'             => 'فیلد :attribute ضروری است.',
    'required_if'          => 'فیلد :attribute ضروری است، وقتی که :other، :value  است.',
    'required_unless'      => 'فیلد :attribute مورد نیاز است مگر اینکه :other، :value مقادیر باشد.',
    'required_with'        => 'فیلد :attribute ضروری است، وقتی که :values موجود باشد.',
    'required_with_all'    => 'فیلد :attribute زمانی که :values وجود دارد.',
    'required_without'     => 'فیلد :attribute ضروری است، وقتی که :values موجود نباشند.',
    'required_without_all' => 'فیلد :attribute وقتی که هیچ یک از :values وجود ندارد، مورد نیاز است.',
    'same'                 => ':attribute و :other باید بر هم منطبق باشند.',
    'size'                 => [
        'numeric' => ':attribute باید به اندازه‌ی :size باشد.',
        'file'    => ':attribute باید به اندازه‌ی :size کیلوبایت باشد.',
        'string'  => ':attribute باید به اندازه‌ی :size کاراکتر باشد.',
        'array'   => ':attribute باید شامل موارد زیر باشد:',
    ],
    'string'               => ':attribute باید یک رشته باشد.',
    'timezone'             => ':attribute باید یک منطقه معتبر باشد.',
    'unique'               => ':attribute در حال حاضر گرفته شده است.',
    'uploaded'             => ':attribute آپلود نشد.',
    'url'                  => 'شکل :attribute نامعتبر است.',
    'unique_undeleted'     => ':attribute باید منحصر به فرد باشد.',
    'non_circular'         => ':attribute نباید یک مدار بسته ایجاد کند',
    'disallow_same_pwd_as_user_fields' => 'رمز عبور نمی تواند همان آدرس ایمیل باشد.',
    'letters'              => 'رمز عبور باید دارای حداقل یک رقم باشد.',
    'numbers'              => 'رمز عبور باید دارای حداقل یک رقم باشد.',
    'case_diff'            => 'رمز عبور باید از حروف مختلط استفاده کند.',
    'symbols'              => 'رمز عبور نباید حاوی فضای خالی باشد.',
    'gte'                  => [
        'numeric'          => 'مقدار نباید منفی باشد.'
    ],


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'alpha_space' => 'فیلد :attribute شامل یک کاراکتر غیر مجاز است.',
        'email_array'      => 'یک یا چند آدرس ایمیل نامعتبر است',
        'hashed_pass'      => 'رمز عبور فعلی شما اشتباه است',
        'dumbpwd'          => 'این رمز عبور خیلی رایج است',
        'statuslabel_type' => 'شما باید نوع برچسب معتبر را انتخاب کنید',

        // date_format validation with slightly less stupid messages. It duplicates a lot, but it gets the job done :(
        // We use this because the default error message for date_format is reflects php Y-m-d, which non-PHP
        // people won't know how to format. 
        'purchase_date.date_format'     => ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',
        'last_audit_date.date_format'   =>  ':attribute باید یک تاریخ معتبر در قالب hh:mm:ss YYYY-MM-DD باشد.',
        'expiration_date.date_format'   =>  ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',
        'termination_date.date_format'  =>  ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',
        'expected_checkin.date_format'  =>  ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',
        'start_date.date_format'        =>  ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',
        'end_date.date_format'          =>  ':attribute باید یک تاریخ معتبر در قالب YYYY-MM-DD باشد.',

    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
