<?php

return array(
    'personal_api_keys' => 'کلیدهای API شخصی',
    'api_key_warning' => 'هنگام ایجاد یک توکن API، مطمئن شوید که آن را فوراً آن را کپی کنید چون دیگر برای شما قابل مشاهده نخواهد بود.',
    'api_base_url' => 'url پایه API شما در این آدرس قرار دارد:',
    'api_base_url_endpoint' => '/&lt;endpoint&gt;

',
    'api_token_expiration_time' => 'توکن‌های API زیر منقضی می‌شوند در:',
    'api_reference' => 'لطفاً <a href="https://api.lanbox.ir/reference" target="_blank">مرجع API</a> را بررسی کنید
                    و مستندات API را پیدا کنید.',
);
