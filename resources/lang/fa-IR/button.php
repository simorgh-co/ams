<?php

return [
    'actions' 	                => 'اقدامات',
    'add'    	                => 'افزودن جدید',
    'cancel'                    => 'انصراف',
    'checkin_and_delete'  	    => 'تحویل گرفتن همه و حذف کاربر',
    'delete'  	                => 'حذف',
    'edit'    	                => 'ويرايش',
    'restore' 	                => 'بازیابی',
    'remove'                    => 'پاک کردن',
    'request'                   => 'درخواست',
    'submit'  	                => 'ثبت کردن',
    'upload'                    => 'آپلود',
    'select_file'				=> 'فایل را انتخاب کنید...',
    'select_files'				=> 'انتخاب فایل ها...',
    'generate_labels'           => '{1} تولید لیبل|[2,*] تولید لیبل ها',
    'send_password_link'        => 'ارسال لینک بازنشانی رمز عبور',
    'go'                        => 'برو',
    'bulk_actions'              => 'اقدام کلی',
    'add_maintenance'           => 'افزودن سابقه تعمیر و نگهداری',
    'append'                    => 'ضمیمه',
    'new'                       => 'جدید',
];
