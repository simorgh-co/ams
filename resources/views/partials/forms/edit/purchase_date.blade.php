<!-- Purchase Date -->
@push('css')
    <link rel="stylesheet" href="{{url(asset('css/persian-datepicker/persian-datepicker.min.css?v=1.1'))}}">
    <link rel="stylesheet" href="{{url(asset('css/persian-datepicker/style.css?v=1.2'))}}">
@endpush

<div id="will-fail" class="form-group {{ $errors->has('purchase_date') ? ' has-error' : '' }}">
    <label for="purchase_date" class="col-md-3 control-label">{{ trans('general.purchase_date') }}</label>

    <div class="col-md-7 col-sm-12">
        <input class="datepicker-demo observer-example-alt2" type="hidden" name="purchase_date_timestamp"
               value="{{old('purchase_date_timestamp')}}"/>
        <input class="form-control" id="date2" name="purchase_date"
               value="{{ old('purchase_date', ($item->purchase_date) ? $item->purchase_date : '') }}"
               placeholder="{{ trans('general.select_date') }}"/>
    </div>
    {!! $errors->first('purchase_date', '<span class="alert-msg" aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i> :message</span>') !!}

</div>

@section('moar_scripts')
    <script src="{{url(asset('js/persian-datepicker/persian-date.min.js?v=1'))}}"></script>
    <script src="{{url(asset('js/persian-datepicker/persian-datepicker.js?v=1.9'))}}"></script>
    <script src="{{url(asset('js/persian-datepicker/holidays.js?v=1.3'))}}"></script>
    <script src="{{url(asset('js/persian-datepicker/datepickerscript.js?v=2.7'))}}"></script>
@stop

