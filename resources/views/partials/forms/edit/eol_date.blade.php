<!-- EOL Date -->
<div class="form-group {{ $errors->has('asset_eol_date') ? ' has-error' : '' }}">
    <label for="date3" class="col-md-3 control-label">{{ trans('admin/hardware/form.eol_date') }}</label>
    <div class="input-group col-md-4">
        <div class="input-group date">
            <input class="datepicker-demo observer-example-alt3" type="hidden" name="asset_eol_date_timestamp"
                   value="{{old('asset_eol_date_timestamp')}}"/>
            <input id="date3" type="text" class="form-control" placeholder="{{ trans('general.select_date') }}"
                   name="asset_eol_date" readonly
                   value="{{ old('asset_eol_date', ($item->asset_eol_date) ? $item->asset_eol_date : '') }}"
                   style="background-color:inherit"/>
        </div>
        {!! $errors->first('asset_eol_date', '<span class="alert-msg" aria-hidden="true"><i class="fas fa-times" aria-hidden="true"></i> :message</span>') !!}
    </div>
</div>
