$(document).ready(function (){

    $('#date1').pDatepicker({
        //altFormat: "YYYY/MM/DD",
        // observer: true,
        altField: '.observer-example-alt1',
        format: 'YYYY/MM/DD',
        initialValue: true,
        initialValueType: 'gregorian',
        autoClose: true,
      //  minDate: new persianDate().valueOf(),
        navigator: {
            text: {
                btnNextText: '⬅️',
                btnPrevText: '➡️',
            },
            onNext: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }

                });
            },
            onPrev: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }
                });
            },
        },
        toolbox: {
            calendarSwitch: {
                enabled: false,
            },
            todayButton: {
                text: {
                    fa: 'برو به امروز',
                },
                onToday: function() {
                    $('td[data-unix]').each(function (index) {
                        var date = new persianDate(parseInt($(this).attr('data-unix')));
                        date.formatPersian = false;
                        if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                            $(this).find('span').addClass('text-danger');
                            // $(this).addClass('disabled');
                        }
                    });
                },
            },
        },
        onShow: function() {
            $('td[data-unix]').each(function (index) {
                var date = new persianDate(parseInt($(this).attr('data-unix')));
                date.formatPersian = false;
                if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                    $(this).find('span').addClass('text-danger');
                    //  $(this).addClass('disabled');
                }
            });
        },

    });

    $('#date2').pDatepicker({
        //altFormat: "YYYY/MM/DD",
        // observer: true,
        altField: '.observer-example-alt2',
        format: 'YYYY/MM/DD',
        initialValue: true,
        initialValueType: 'gregorian',
        autoClose: true,
        //  minDate: new persianDate().valueOf(),
        navigator: {
            text: {
                btnNextText: '⬅️',
                btnPrevText: '➡️',
            },
            onNext: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }

                });
            },
            onPrev: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }
                });
            },
        },
        toolbox: {
            calendarSwitch: {
                enabled: false,
            },
            todayButton: {
                text: {
                    fa: 'برو به امروز',
                },
                onToday: function() {
                    $('td[data-unix]').each(function (index) {
                        var date = new persianDate(parseInt($(this).attr('data-unix')));
                        date.formatPersian = false;
                        if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                            $(this).find('span').addClass('text-danger');
                            // $(this).addClass('disabled');
                        }
                    });
                },
            },
        },
        onShow: function() {
            $('td[data-unix]').each(function (index) {
                var date = new persianDate(parseInt($(this).attr('data-unix')));
                date.formatPersian = false;
                if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                    $(this).find('span').addClass('text-danger');
                    //  $(this).addClass('disabled');
                }
            });
        },

    });

    $('#date3').pDatepicker({
        //altFormat: "YYYY/MM/DD",
        // observer: true,
        altField: '.observer-example-alt3',
        format: 'YYYY/MM/DD',
        initialValue: true,
        initialValueType: 'gregorian',
        autoClose: true,
        //  minDate: new persianDate().valueOf(),
        navigator: {
            text: {
                btnNextText: '⬅️',
                btnPrevText: '➡️',
            },
            onNext: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }

                });
            },
            onPrev: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }
                });
            },
        },
        toolbox: {
            calendarSwitch: {
                enabled: false,
            },
            todayButton: {
                text: {
                    fa: 'برو به امروز',
                },
                onToday: function() {
                    $('td[data-unix]').each(function (index) {
                        var date = new persianDate(parseInt($(this).attr('data-unix')));
                        date.formatPersian = false;
                        if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                            $(this).find('span').addClass('text-danger');
                            // $(this).addClass('disabled');
                        }
                    });
                },
            },
        },
        onShow: function() {
            $('td[data-unix]').each(function (index) {
                var date = new persianDate(parseInt($(this).attr('data-unix')));
                date.formatPersian = false;
                if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                    $(this).find('span').addClass('text-danger');
                    //  $(this).addClass('disabled');
                }
            });
        },

    });
    $('.persian-calender').pDatepicker({
        //altFormat: "YYYY/MM/DD",
        // observer: true,
        altField: '.observer-example-alt3',
        format: 'YYYY/MM/DD',
        initialValue: true,
        initialValueType: 'gregorian',
        autoClose: true,
        //  minDate: new persianDate().valueOf(),
        navigator: {
            text: {
                btnNextText: '⬅️',
                btnPrevText: '➡️',
            },
            onNext: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }

                });
            },
            onPrev: function() {
                $('td[data-unix]').each(function (index) {
                    var date = new persianDate(parseInt($(this).attr('data-unix')));
                    date.formatPersian = false;
                    if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                        $(this).find('span').addClass('text-danger');
                        // $(this).addClass('disabled');
                    }
                });
            },
        },
        toolbox: {
            calendarSwitch: {
                enabled: false,
            },
            todayButton: {
                text: {
                    fa: 'برو به امروز',
                },
                onToday: function() {
                    $('td[data-unix]').each(function (index) {
                        var date = new persianDate(parseInt($(this).attr('data-unix')));
                        date.formatPersian = false;
                        if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                            $(this).find('span').addClass('text-danger');
                            // $(this).addClass('disabled');
                        }
                    });
                },
            },
        },
        onShow: function() {
            $('td[data-unix]').each(function (index) {
                var date = new persianDate(parseInt($(this).attr('data-unix')));
                date.formatPersian = false;
                if(holidays.includes(date.format('YYYY/MM/DD')) || date.format('d') == 7) {
                    $(this).find('span').addClass('text-danger');
                    //  $(this).addClass('disabled');
                }
            });
        },

    });


});

